package com.acelity.customer.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import com.acelity.customer.dao.CustomerDAO;
import com.acelity.customer.model.*;

@javax.jws.WebService(name = "CustomerWS", serviceName = "CustomerWS", targetNamespace = "http://com.kci.cache/customerCache")

public class CustomerWS {
	private CustomerDAO customerDAO;

	@WebMethod(exclude = true)
	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	@WebMethod(operationName = "getCustomerByCustomerID")
	public List<XxkciPegaCustomerIdsV> findAllgetCustomerByCustomerIDCustomers(
			@WebParam(name = "customerID") Long customerID) {
		return customerDAO.getCustomerByCustomerID(customerID);
	}

	@WebMethod(operationName = "getPatient")
	public List<XxkciPegaCustomerIdsV> getPatient(
			@WebParam(name = "customerNumber") String customerNumber,
			@WebParam(name = "customerName") String customerName, @WebParam(name = "address1") String address1,
			@WebParam(name = "address2") String address2, @WebParam(name = "city") String city,
			@WebParam(name = "state") String state, @WebParam(name = "postalCode") String postalCode) {
		return customerDAO.getPatient(customerNumber, customerName, address1, address2, city,
				state, postalCode);
	}
	
	@WebMethod(operationName = "getPayor")
	public List<XxkciPegaPayorIdsV> getPayor(
			@WebParam(name = "customerNumber") String customerNumber,
			@WebParam(name = "customerName") String customerName, @WebParam(name = "address1") String address1,
			@WebParam(name = "address2") String address2, @WebParam(name = "city") String city,
			@WebParam(name = "state") String state, @WebParam(name = "postalCode") String postalCode) {
		return customerDAO.getPayor(customerNumber, customerName, address1, address2, city,
				state, postalCode);
	}
	
	@WebMethod(operationName = "getCareGiver")
	public List<XxkciPegaCaregiverIdsV> getCareGiver(
			@WebParam(name = "customerNumber") String customerNumber,
			@WebParam(name = "customerName") String customerName, @WebParam(name = "address1") String address1,
			@WebParam(name = "address2") String address2, @WebParam(name = "city") String city,
			@WebParam(name = "state") String state, @WebParam(name = "postalCode") String postalCode,
			@WebParam(name = "phone") String phone, @WebParam(name = "fax") String fax) {
		return customerDAO.getCareGiver(customerNumber, customerName, address1, address2,city, state, postalCode, phone, fax);
	}

}
