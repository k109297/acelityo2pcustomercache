package com.acelity.customer.dao.encrypt;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.acelity.cryptography.AcelityEncryptDecrypt;

import sun.misc.BASE64Decoder;

public class EncryptedDataSource extends DriverManagerDataSource {

	private static SecretKeySpec secretKey;
	private static byte[] key;
	//private static final String keystr = "rytc8e67rbfo2e3x";
 
	@Override
	public String getPassword() {
		String password = super.getPassword();
		//System.out.println("Password"+decrypt(password, keystr));
		AcelityEncryptDecrypt aed = new AcelityEncryptDecrypt();
		return aed.decrypt256(password, System.getProperty("DCRYPT_KEY"), System.getProperty("DCRYPT_SALT"));
	}
	
	@Override
	public String getUsername() {
		String username = super.getUsername();
		//System.out.println("getUsername"+decrypt(username, keystr));
		AcelityEncryptDecrypt aed = new AcelityEncryptDecrypt();
		return aed.decrypt256(username, System.getProperty("DCRYPT_KEY"), System.getProperty("DCRYPT_SALT"));
	}
}
