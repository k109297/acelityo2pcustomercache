package com.acelity.customer.dao;

import java.util.List;

import com.acelity.customer.model.XxkciPegaCaregiverIdsV;
import com.acelity.customer.model.XxkciPegaCustomerIdsV;
import com.acelity.customer.model.XxkciPegaPayorIdsV;

public interface CustomerDAO {

	List<XxkciPegaCustomerIdsV> getCustomerByCustomerID(Long customerID);
	List<XxkciPegaCustomerIdsV> getPatient(String customerNo, String customerName, String address1, String address2, String city, String state, String postalCode);
	List<XxkciPegaPayorIdsV> getPayor(String customerNo, String customerName, String address1, String address2, String city, String state, String postalCode);
	List<XxkciPegaCaregiverIdsV> getCareGiver(String masterId, String customerName, String address1,  String address2, String city, String state, String postalCode, String phone, String fax);
	 
}
