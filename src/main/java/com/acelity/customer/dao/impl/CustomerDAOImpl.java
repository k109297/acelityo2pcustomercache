package com.acelity.customer.dao.impl;

import java.util.Collections;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.acelity.customer.dao.CustomerDAO;
import com.acelity.customer.model.XxkciPegaCaregiverIdsV;
import com.acelity.customer.model.XxkciPegaCustomerIdsV;
import com.acelity.customer.model.XxkciPegaPayorIdsV;


public class CustomerDAOImpl implements CustomerDAO{
	
	private SessionFactory sessionFactory;
	private static final int QUERY_TIMEOUT=100000000;
	public void setSessionFactory(SessionFactory sessionFactory) 
	{
	    this.sessionFactory = sessionFactory;
	}
	public SessionFactory  getSessionFactory() {
		return this.sessionFactory;
	}
	
	 public List<XxkciPegaCustomerIdsV> getCustomerByCustomerID(Long customerID) {
	        Session session = sessionFactory.openSession();
	        Query query = session.createQuery("from XxkciPegaCustomerIdsV o where 1=1");
	        query.setParameter("customerID", customerID);
	        query.setTimeout(1000000);
	        List<XxkciPegaCustomerIdsV> employees =  query.list();
	        session.close();
	        if(employees != null && !employees.isEmpty()){
	            return employees;
	        }
	        return Collections.emptyList();
	    }
 
	 public List<XxkciPegaCustomerIdsV> getPatient(String customerNumber, String customerName, String address1, String address2, String city, String state, String zipCode){
	        Session session = sessionFactory.openSession();
	        
	        StringBuffer hql=new StringBuffer("from XxkciPegaCustomerIdsV o where 1=1 ");
	      
	        if(customerNumber!=null && !customerNumber.isEmpty())
	        {
	        	hql.append(" and customerNumber like :customerNumber");
	        }
	        
	        
	        if(customerName!=null && !customerName.isEmpty())
	        {
	        	hql.append(" and customerName like :customerName");
	        }
	        
	        
	        if(address1!=null && !address1.isEmpty())
	        {
	        	hql.append(" and address1 like :address1");
	        }
	      
	        
	        if(address2!=null && !address2.isEmpty())
	        {
	        	hql.append(" and address2 like :address2");
	        }
	        
	        
	        if(city!=null && !city.isEmpty())
	        {
	        	hql.append(" and city like :city");
	        }
	       
	        
	        if(state!=null && !state.isEmpty())
	        {
	        	hql.append(" and state like :state");
	        }
	      
	        
	        if(zipCode!=null && !zipCode.isEmpty())
	        {
	        	hql.append(" and zipCode like :zipCode");
	        }
	            
	        
	        hql.append("  order by customerName, city, state");
	        
	        Query query = session.createQuery(hql.toString());
	        
	        if(customerNumber!=null && !customerNumber.isEmpty())
	        {
	        	query.setParameter("customerNumber",customerNumber+"%");
	        }
	        
	        
	        if(customerName!=null && !customerName.isEmpty())
	        {
	        	query.setParameter("customerName",customerName+"%");
	        }
	        
	        
	        if(address1!=null && !address1.isEmpty())
	        {
	        	query.setParameter("address1",address1+"%");
	        }
	      
	        
	        if(address2!=null && !address2.isEmpty())
	        {
	        	query.setParameter("address2",address2+"%");
	        }
	        
	        
	        if(city!=null && !city.isEmpty())
	        {
	        	query.setParameter("city",city+"%");
	        }
	       
	        
	        if(state!=null && !state.isEmpty())
	        {
	        	query.setParameter("state",state+"%");
	        }
	      
	        
	        if(zipCode!=null && !zipCode.isEmpty())
	        {
	        	query.setParameter("zipCode",zipCode+"%");
	        }
	        
	        
		              
	        List<XxkciPegaCustomerIdsV> patients =  query.list();
	        session.close();
	        if(patients != null && !patients.isEmpty())
	        {
	            return patients;
	        }
	        return Collections.emptyList();
	    }
	
	 
	 public List<XxkciPegaCustomerIdsV> getPatientWithCriteria(String customerNumber, String customerName, String address1, String address2, String city, String state, String postalCode){
	        Session session = sessionFactory.openSession();
	    
	        Criteria crit  = session.createCriteria(XxkciPegaCustomerIdsV.class);
	      
	        if(customerNumber!=null && !customerNumber.isEmpty())
	        {
	        	crit.add(Restrictions.ilike("customerNumber",customerNumber+"%",MatchMode.END));
	        }
	        
	        
	        if(customerName!=null && !customerName.isEmpty())
	        {
	        	crit.add(Restrictions.ilike("customerName", customerName+"%",MatchMode.END));
	        }
	        
	        
	        if(address1!=null && !address1.isEmpty())
	        {
	        	  crit.add(Restrictions.ilike("address1", address1+"%",MatchMode.END));
	        }
	      
	        
	        if(address2!=null && !address2.isEmpty())
	        {
	        	crit.add(Restrictions.ilike("address2", address2+"%",MatchMode.END));
	        }
	        
	        
	        if(city!=null && !city.isEmpty())
	        {
	        	 crit.add(Restrictions.ilike("city", city+"%",MatchMode.END));
	        }
	       
	        
	        if(state!=null && !state.isEmpty())
	        {
	        	  crit.add(Restrictions.ilike("state", state+"%",MatchMode.END));
	        }
	      
	        
	        if(postalCode!=null && !postalCode.isEmpty())
	        {
	        	 crit.add(Restrictions.ilike("zipCode", postalCode+"%",MatchMode.END));
	        }
	              
	        crit.addOrder(Order.asc("customerName")).addOrder(Order.asc("city")).addOrder(Order.asc("state"));
		              
	        List<XxkciPegaCustomerIdsV> patients =  crit.list();
	        session.close();
	        if(patients != null && !patients.isEmpty())
	        {
	            return patients;
	        }
	        return Collections.emptyList();
	    }
	 
	
	 
	 public List<XxkciPegaCaregiverIdsV> getCareGiverWithCriteria(String mstrId, String customerName, String address1, String address2, String city, String state, String postalCode, String phone, String fax){
	        Session session = sessionFactory.openSession();
	       
	        
	        Criteria crit  = session.createCriteria(XxkciPegaCaregiverIdsV.class);
	 
	        if(mstrId!=null && !mstrId.isEmpty())
	        crit.add(Restrictions.ilike("mstrId",mstrId+"%",MatchMode.ANYWHERE));
	        
	        if(customerName!=null && !customerName.isEmpty())
	        crit.add(Restrictions.ilike("name1",customerName+"%",MatchMode.ANYWHERE));
	        
	        if(address1!=null && !address1.isEmpty())
	        crit.add(Restrictions.ilike("add1",address1+"%",MatchMode.ANYWHERE));
	        
	        if(address2!=null && !address2.isEmpty())
	        crit.add(Restrictions.ilike("add2",address2+"%",MatchMode.ANYWHERE));
	        
	        if(city!=null && !city.isEmpty())
	        crit.add(Restrictions.ilike("city",city+"%",MatchMode.ANYWHERE));
	        
	        if(state!=null && !state.isEmpty())
	        crit.add(Restrictions.ilike("state",state+"%",MatchMode.ANYWHERE));
	        
	        if(postalCode!=null && !postalCode.isEmpty())
	        crit.add(Restrictions.ilike("zipCode",postalCode+"%",MatchMode.ANYWHERE));
	        
	        if(phone!=null && !phone.isEmpty())
	        crit.add(Restrictions.ilike("phone",phone+"%",MatchMode.ANYWHERE));
	        
	        if(fax!=null && !fax.isEmpty())
	        crit.add(Restrictions.ilike("fax",fax+"%",MatchMode.ANYWHERE));
	        
	        crit.addOrder(Order.asc("name1")).addOrder(Order.asc("city")).addOrder(Order.asc("state"));
	        
	        
	        List<XxkciPegaCaregiverIdsV> careGivers =  crit.list();
	        
	        session.close();
	        if(careGivers != null && !careGivers.isEmpty()){
	            return careGivers;
	        }
	        return Collections.emptyList();
	    }
	 
	 public List<XxkciPegaCaregiverIdsV> getCareGiver(String mstrId, String name1, String add1, String add2, String city, String state, String zipCode, String phone, String fax){
	        Session session = sessionFactory.openSession();
	       
	        
	        StringBuffer hql=new StringBuffer("from XxkciPegaCaregiverIdsV o where 1=1 ");
		      
	        if(mstrId!=null && !mstrId.isEmpty())
	        {
	        	hql.append(" and mstrId like :mstrId");
	        }
	        
	        
	        if(name1!=null && !name1.isEmpty())
	        {
	        	hql.append(" and name1 like :name1");
	        }
	        
	        
	        if(add1!=null && !add1.isEmpty())
	        {
	        	hql.append(" and add1 like :add1");
	        }
	      
	        
	        if(add2!=null && !add2.isEmpty())
	        {
	        	hql.append(" and add2 like :add2");
	        }
	        
	        
	        if(city!=null && !city.isEmpty())
	        {
	        	hql.append(" and city like :city");
	        }
	       
	        
	        if(state!=null && !state.isEmpty())
	        {
	        	hql.append(" and state like :state");
	        }
	      
	        
	        if(zipCode!=null && !zipCode.isEmpty())
	        {
	        	hql.append(" and zipCode like :zipCode");
	        }
	        
	        if(phone!=null && !phone.isEmpty())
	        {
	        	hql.append(" and phone like :phone");
	        }
	        
	        
	        if(fax!=null && !fax.isEmpty())
	        {
	        	hql.append(" and fax like :fax");
	        }
	        
	        hql.append("  order by name1, city, state");
	        
	        Query query = session.createQuery(hql.toString());
	        
	        if(mstrId!=null && !mstrId.isEmpty())
	        {
	        	query.setParameter("mstrId",mstrId+"%");
	        }
	        
	        
	        if(name1!=null && !name1.isEmpty())
	        {
	        	query.setParameter("name1",name1+"%");
	        }
	        
	        
	        if(add1!=null && !add1.isEmpty())
	        {
	        	query.setParameter("add1",add1+"%");
	        }
	      
	        
	        if(add2!=null && !add2.isEmpty())
	        {
	        	query.setParameter("add2",add2+"%");
	        }
	        
	        
	        if(city!=null && !city.isEmpty())
	        {
	        	query.setParameter("city",city+"%");
	        }
	       
	        
	        if(state!=null && !state.isEmpty())
	        {
	        	query.setParameter("state",state+"%");
	        }
	      
	        
	        if(zipCode!=null && !zipCode.isEmpty())
	        {
	        	query.setParameter("zipCode",zipCode+"%");
	        }
	        
	        if(phone!=null && !phone.isEmpty())
	        {
	        	query.setParameter("phone",phone+"%");
	        }
	        
	        if(fax!=null && !fax.isEmpty())
	        {
	        	query.setParameter("fax",fax+"%");
	        }
		              
	        List<XxkciPegaCaregiverIdsV> cgs =  query.list();
	        session.close();
	        if(cgs != null && !cgs.isEmpty())
	        {
	            return cgs;
	        }
	        return Collections.emptyList();
	        
	    }
	 
	 public List<XxkciPegaPayorIdsV> getPayorWithCriteria(Long customerNumber, String customerName, String address1, String address2, String city, String state, String postalCode){
	        Session session = sessionFactory.openSession();
	        
	        Criteria crit  = session.createCriteria(XxkciPegaPayorIdsV.class);
	         
	        if(customerNumber!=null)
	        crit.add(Restrictions.eq("custAccountId",customerNumber));
	        
	        if(customerName!=null && !customerName.isEmpty())
	        crit.add(Restrictions.ilike("partyName",customerName+"%",MatchMode.ANYWHERE));
	        
	        if(address1!=null && !address1.isEmpty())
	        crit.add(Restrictions.ilike("address1",address1+"%",MatchMode.ANYWHERE));
	        
	        if(address2!=null && !address2.isEmpty())
	        crit.add(Restrictions.ilike("address2",address2+"%",MatchMode.ANYWHERE));
	        
	        if(city!=null && !city.isEmpty())
	        crit.add(Restrictions.ilike("city",city+"%",MatchMode.ANYWHERE));
	        
	        if(state!=null && !state.isEmpty())
	        crit.add(Restrictions.ilike("state",state+"%",MatchMode.ANYWHERE));
	        
	        if(postalCode!=null && !postalCode.isEmpty())
	        crit.add(Restrictions.ilike("postalCode",postalCode+"%",MatchMode.ANYWHERE));
	        
	        crit.addOrder(Order.asc("partyName")).addOrder(Order.asc("city")).addOrder(Order.asc("state"));
		      
	        
	        List<XxkciPegaPayorIdsV> payors =  crit.list();
	        session.close();
	        if(payors != null && !payors.isEmpty()){
	            return payors;
	        }
	        return Collections.emptyList();
	    }
	 
	 public List<XxkciPegaPayorIdsV> getPayor(String custAccountId, String partyName, String address1, String address2, String city, String state, String postalCode){
		 Session session = sessionFactory.openSession();
	       
	        
	        StringBuffer hql=new StringBuffer("from XxkciPegaPayorIdsV o where 1=1 ");
		      
	        if(custAccountId!=null && !custAccountId.isEmpty())
	        {
	        	hql.append(" and custAccountId = :custAccountId");
	        }
	        
	        
	        if(partyName!=null && !partyName.isEmpty())
	        {
	        	hql.append(" and partyName like :partyName");
	        }
	        
	        
	        if(address1!=null && !address1.isEmpty())
	        {
	        	hql.append(" and address1 like :address1");
	        }
	      
	        
	        if(address2!=null && !address2.isEmpty())
	        {
	        	hql.append(" and address2 like :address2");
	        }
	        
	        
	        if(city!=null && !city.isEmpty())
	        {
	        	hql.append(" and city like :city");
	        }
	       
	        
	        if(state!=null && !state.isEmpty())
	        {
	        	hql.append(" and state like :state");
	        }
	      
	        
	        if(postalCode!=null && !postalCode.isEmpty())
	        {
	        	hql.append(" and postalCode like :postalCode");
	        }
	        
	        hql.append("  order by partyName, city, state");
	        
	        Query query = session.createQuery(hql.toString());
	        
	        if(custAccountId!=null && !custAccountId.isEmpty())
	        {
	        	query.setParameter("custAccountId",Long.parseLong(custAccountId));
	        }
	        
	        
	        if(partyName!=null && !partyName.isEmpty())
	        {
	        	query.setParameter("partyName",partyName+"%");
	        }
	        
	        
	        if(address1!=null && !address1.isEmpty())
	        {
	        	query.setParameter("address1",address1+"%");
	        }
	      
	        
	        if(address2!=null && !address2.isEmpty())
	        {
	        	query.setParameter("address2",address2+"%");
	        }
	        
	        
	        if(city!=null && !city.isEmpty())
	        {
	        	query.setParameter("city",city+"%");
	        }
	       
	        
	        if(state!=null && !state.isEmpty())
	        {
	        	query.setParameter("state",state+"%");
	        }
	      
	        
	        if(postalCode!=null && !postalCode.isEmpty())
	        {
	        	query.setParameter("postalCode",postalCode+"%");
	        }
	        
	        
		              
	        List<XxkciPegaPayorIdsV> pg =  query.list();
	        session.close();
	        if(pg != null && !pg.isEmpty())
	        {
	            return pg;
	        }
	        return Collections.emptyList();
	    }

}
