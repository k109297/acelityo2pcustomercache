package com.acelity.customer.dao;

 
import javax.xml.ws.Endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.acelity.customer.ws.CustomerWS;
public class CustomerRun {
	private static Logger logger = null;  
    public static void main(String[] args) {
    	logger = LoggerFactory.getLogger(CustomerRun.class);
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-core.xml");
        CustomerWS service = (CustomerWS) ctx.getBean("serviceBean");

        Endpoint.publish("http://0.0.0.0:7001/customerCache", service);
        logger.info("Server start in Port .. 7001");
    }
}