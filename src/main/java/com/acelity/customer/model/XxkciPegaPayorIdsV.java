package com.acelity.customer.model;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({ @NamedQuery(name = "XxkciPegaPayorIdsV.findAll", query = "select o from XxkciPegaPayorIdsV o") })
@Table(name = "APPS.XXKCI_PEGA_PAYOR_IDS_V")
@XmlRootElement(name = "XxkciPegaPayorIdsV")
@XmlAccessorType (XmlAccessType.FIELD)
public class XxkciPegaPayorIdsV implements Serializable {
    private static final long serialVersionUID = 3116764809161823350L;
    @Id
   	@Column(name = "ID",  updatable =false)
    public String ID;
    @Column(name = "ACCOUNT_NUMBER", nullable = false, length = 30)
    private String accountNumber;
    @Column(length = 25)
    private String address1;
    @Column(length = 25)
    private String address2;
    @Column(length = 25)
    private String address3;
    @Column(length = 25)
    private String address4;
    @Column(length = 150)
    private String attribute6;
    @Column(length = 150)
    private String attribute7;
    @Column(length = 150)
    private String attribute8;
    @Column(length = 60)
    private String city;
    @Column(length = 15)
    private String county;
    @Column(name = "CUSTOMER_CLASS_CODE", length = 30)
    private String customerClassCode;
    @Column(name = "CUST_ACCOUNT_ID", nullable = false)
    private Long custAccountId;
    @Column(name = "CUST_ACCT_SITE_ID", nullable = false)
    private Long custAcctSiteId;
    @Column(name = "GLOBAL_LOCATION_NUMBER", length = 40)
    private String globalLocationNumber;
   
    @Column(nullable = false)
    private Long inssub;
    @Column(name = "ORG_ID")
    private Long orgId;
    @Column(name = "PARTY_NAME", nullable = false, length = 360)
    private String partyName;
    @Column(name = "PARTY_TYPE", nullable = false, length = 30)
    private String partyType;
    @Column(name = "POSTAL_CODE", length = 15)
    private String postalCode;
    @Column(name = "PRIMARY_BILLTO")
    private String primaryBillto;
    @Column(name = "SITE_USE_ID", nullable = false)
    private Long siteUseId;
    @Column(length = 3)
    private String state;
    @Column(nullable = false, length = 1)
    private String status;

    public XxkciPegaPayorIdsV() {
    }

    public XxkciPegaPayorIdsV(String accountNumber, String address1, String address2, String address3, String address4,
                              String attribute6, String attribute7, String attribute8, String city, String county,
                              Long custAccountId, Long custAcctSiteId, String customerClassCode,
                              String globalLocationNumber, Long inssub, Long orgId, String partyName, String partyType,
                              String postalCode, String primaryBillto, Long siteUseId, String state, String status) {
        this.accountNumber = accountNumber;
        this.address1 = address1;
        this.address2 = address2;
        this.address3 = address3;
        this.address4 = address4;
        this.attribute6 = attribute6;
        this.attribute7 = attribute7;
        this.attribute8 = attribute8;
        this.city = city;
        this.county = county;
        this.custAccountId = custAccountId;
        this.custAcctSiteId = custAcctSiteId;
        this.customerClassCode = customerClassCode;
        this.globalLocationNumber = globalLocationNumber;
        this.inssub = inssub;
        this.orgId = orgId;
        this.partyName = partyName;
        this.partyType = partyType;
        this.postalCode = postalCode;
        this.primaryBillto = primaryBillto;
        this.siteUseId = siteUseId;
        this.state = state;
        this.status = status;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getAttribute6() {
        return attribute6;
    }

    public void setAttribute6(String attribute6) {
        this.attribute6 = attribute6;
    }

    public String getAttribute7() {
        return attribute7;
    }

    public void setAttribute7(String attribute7) {
        this.attribute7 = attribute7;
    }

    public String getAttribute8() {
        return attribute8;
    }

    public void setAttribute8(String attribute8) {
        this.attribute8 = attribute8;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCustomerClassCode() {
        return customerClassCode;
    }

    public void setCustomerClassCode(String customerClassCode) {
        this.customerClassCode = customerClassCode;
    }

    public Long getCustAccountId() {
        return custAccountId;
    }

    public void setCustAccountId(Long custAccountId) {
        this.custAccountId = custAccountId;
    }

    public Long getCustAcctSiteId() {
        return custAcctSiteId;
    }

    public void setCustAcctSiteId(Long custAcctSiteId) {
        this.custAcctSiteId = custAcctSiteId;
    }

    public String getGlobalLocationNumber() {
        return globalLocationNumber;
    }

    public void setGlobalLocationNumber(String globalLocationNumber) {
        this.globalLocationNumber = globalLocationNumber;
    }

    public String getId() {
        return ID;
    }

    public void setId(String id) {
        this.ID = id;
    }

    public Long getInssub() {
        return inssub;
    }

    public void setInssub(Long inssub) {
        this.inssub = inssub;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getPartyType() {
        return partyType;
    }

    public void setPartyType(String partyType) {
        this.partyType = partyType;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPrimaryBillto() {
        return primaryBillto;
    }

    public void setPrimaryBillto(String primaryBillto) {
        this.primaryBillto = primaryBillto;
    }

    public Long getSiteUseId() {
        return siteUseId;
    }

    public void setSiteUseId(Long siteUseId) {
        this.siteUseId = siteUseId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
