package com.acelity.customer.model;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({ @NamedQuery(name = "XxkciPegaCustomerIdsV.findAll", query = "select o from XxkciPegaCustomerIdsV o") })
@Table(name = "APPS.XXKCI_PEGA_CUSTOMER_IDS_V")
@XmlRootElement(name = "XxkciPegaCustomerIdsV")
@XmlAccessorType (XmlAccessType.FIELD)
public class XxkciPegaCustomerIdsV implements Serializable {
    private static final long serialVersionUID = 1420106109121757425L;
    @Id
	@Column(name = "ID",  updatable =false)
 	public String ID;
    @Column(name = "ACCOUNT_TYPE_CODE", length = 240)
    private String accountTypeCode;
    @Column(name = "ADDRESS_1", nullable = false, length = 240)
    private String address1;
    @Column(name = "ADDRESS_2", length = 240)
    private String address2;
    @Column(name = "ADDRESS_ID", nullable = false)
    private Long addressId;
    @Column(name = "ADDRESS_STATUS", nullable = false, length = 1)
    private String addressStatus;
    @Column(length = 150)
    private String attribute5;
    @Column(length = 150)
    private String attribute6;
    @Column(name = "BILL_TO_SITE_USE_ID")
    private Long billToSiteUseId;
    @Column(length = 60)
    private String city;
    @Column(nullable = false, length = 60)
    private String country;
    @Column(length = 60)
    private String county;
    @Column(name = "CUST_ACCT_SITE_ID", nullable = false)
    private Long custAcctSiteId;
    @Column(name = "CUSTOMER_CATEGORY_CODE", length = 30)
    private String customerCategoryCode;
    @Column(name = "CUSTOMER_CLASS_CODE", length = 30)
    private String customerClassCode;
    @Column(name = "CUSTOMER_ID", nullable = false)
    private Long customerId;
    @Column(name = "CUSTOMER_NAME", length = 50)
    private String customerName;
    @Column(name = "CUSTOMER_NUMBER", nullable = false, length = 30)
    private String customerNumber;
    @Column(name = "CUSTOMER_STATUS", nullable = false, length = 1)
    private String customerStatus;
    @Column(nullable = false, length = 40)
    private String location;
    @Column(name = "MARKETING_SEGMENT_CODE", length = 150)
    private String marketingSegmentCode;
    @Column(name = "ORG_ID")
    private Long orgId;
    @Column(name = "ORIG_SYSTEM_REFERENCE", nullable = false, length = 240)
    private String origSystemReference;
    @Column(name = "PRIMARY_FLAG", nullable = false, length = 1)
    private String primaryFlag;
    @Column(name = "RC_PRICE_LIST_ID")
    private Long rcPriceListId;
    @Column(name = "RSU_PRICE_LIST_ID")
    private Long rsuPriceListId;
    @Column(name = "SERVICE_CENTER_ID", length = 3)
    private String serviceCenterId;
    @Column(name = "SITE_USE_CODE", nullable = false, length = 30)
    private String siteUseCode;
    @Column(name = "SITE_USE_ID", nullable = false)
    private Long siteUseId;
    @Column(length = 60)
    private String state;
    @Column(nullable = false, length = 1)
    private String status;
    @Column(name = "ZIP_CODE", length = 60)
    private String zipCode;

    public XxkciPegaCustomerIdsV() {
    }

    public XxkciPegaCustomerIdsV(String accountTypeCode, String address1, String address2, Long addressId,
                                 String addressStatus, String attribute5, String attribute6, Long billToSiteUseId,
                                 String city, String country, String county, Long custAcctSiteId,
                                 String customerCategoryCode, String customerClassCode, Long customerId,
                                 String customerName, String customerNumber, String customerStatus, String location,
                                 String marketingSegmentCode, Long orgId, String origSystemReference,
                                 String primaryFlag, Long rcPriceListId, Long rsuPriceListId, String serviceCenterId,
                                 String siteUseCode, Long siteUseId, String state, String status, String zipCode) {
        this.accountTypeCode = accountTypeCode;
        this.address1 = address1;
        this.address2 = address2;
        this.addressId = addressId;
        this.addressStatus = addressStatus;
        this.attribute5 = attribute5;
        this.attribute6 = attribute6;
        this.billToSiteUseId = billToSiteUseId;
        this.city = city;
        this.country = country;
        this.county = county;
        this.custAcctSiteId = custAcctSiteId;
        this.customerCategoryCode = customerCategoryCode;
        this.customerClassCode = customerClassCode;
        this.customerId = customerId;
        this.customerName = customerName;
        this.customerNumber = customerNumber;
        this.customerStatus = customerStatus;
        this.location = location;
        this.marketingSegmentCode = marketingSegmentCode;
        this.orgId = orgId;
        this.origSystemReference = origSystemReference;
        this.primaryFlag = primaryFlag;
        this.rcPriceListId = rcPriceListId;
        this.rsuPriceListId = rsuPriceListId;
        this.serviceCenterId = serviceCenterId;
        this.siteUseCode = siteUseCode;
        this.siteUseId = siteUseId;
        this.state = state;
        this.status = status;
        this.zipCode = zipCode;
    }

    public String getAccountTypeCode() {
        return accountTypeCode;
    }

    public void setAccountTypeCode(String accountTypeCode) {
        this.accountTypeCode = accountTypeCode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public String getAddressStatus() {
        return addressStatus;
    }

    public void setAddressStatus(String addressStatus) {
        this.addressStatus = addressStatus;
    }

    public String getAttribute5() {
        return attribute5;
    }

    public void setAttribute5(String attribute5) {
        this.attribute5 = attribute5;
    }

    public String getAttribute6() {
        return attribute6;
    }

    public void setAttribute6(String attribute6) {
        this.attribute6 = attribute6;
    }

    public Long getBillToSiteUseId() {
        return billToSiteUseId;
    }

    public void setBillToSiteUseId(Long billToSiteUseId) {
        this.billToSiteUseId = billToSiteUseId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public Long getCustAcctSiteId() {
        return custAcctSiteId;
    }

    public void setCustAcctSiteId(Long custAcctSiteId) {
        this.custAcctSiteId = custAcctSiteId;
    }

    public String getCustomerCategoryCode() {
        return customerCategoryCode;
    }

    public void setCustomerCategoryCode(String customerCategoryCode) {
        this.customerCategoryCode = customerCategoryCode;
    }

    public String getCustomerClassCode() {
        return customerClassCode;
    }

    public void setCustomerClassCode(String customerClassCode) {
        this.customerClassCode = customerClassCode;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getId() {
        return ID;
    }

    public void setId(String id) {
        this.ID = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMarketingSegmentCode() {
        return marketingSegmentCode;
    }

    public void setMarketingSegmentCode(String marketingSegmentCode) {
        this.marketingSegmentCode = marketingSegmentCode;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrigSystemReference() {
        return origSystemReference;
    }

    public void setOrigSystemReference(String origSystemReference) {
        this.origSystemReference = origSystemReference;
    }

    public String getPrimaryFlag() {
        return primaryFlag;
    }

    public void setPrimaryFlag(String primaryFlag) {
        this.primaryFlag = primaryFlag;
    }

    public Long getRcPriceListId() {
        return rcPriceListId;
    }

    public void setRcPriceListId(Long rcPriceListId) {
        this.rcPriceListId = rcPriceListId;
    }

    public Long getRsuPriceListId() {
        return rsuPriceListId;
    }

    public void setRsuPriceListId(Long rsuPriceListId) {
        this.rsuPriceListId = rsuPriceListId;
    }

    public String getServiceCenterId() {
        return serviceCenterId;
    }

    public void setServiceCenterId(String serviceCenterId) {
        this.serviceCenterId = serviceCenterId;
    }

    public String getSiteUseCode() {
        return siteUseCode;
    }

    public void setSiteUseCode(String siteUseCode) {
        this.siteUseCode = siteUseCode;
    }

    public Long getSiteUseId() {
        return siteUseId;
    }

    public void setSiteUseId(Long siteUseId) {
        this.siteUseId = siteUseId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
