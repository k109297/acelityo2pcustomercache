package com.acelity.customer.model;


import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({ @NamedQuery(name = "XxkciPegaCaregiverIdsV.findAll",
                            query = "select o from XxkciPegaCaregiverIdsV o") })
@Table(name = "APPS.XXKCI_PEGA_CAREGIVER_IDS_V")
@XmlRootElement(name = "XxkciPegaCaregiverIdsV")
@XmlAccessorType (XmlAccessType.FIELD)
public class XxkciPegaCaregiverIdsV implements Serializable {
    private static final long serialVersionUID = 2402547377650024468L;
    @Id
   	@Column(name = "ID",  updatable =false)
    public String ID;
    @Column(name = "ADD_1", length = 35)
    private String add1;
    @Column(name = "ADD_2", length = 35)
    private String add2;
    @Column(name = "ADD_DESC")
    private String addDesc;
    @Column(name = "ADD_TYPE")
    private String addType;
    @Column(name = "BUS_TYPE", length = 3)
    private String busType;
    @Column(length = 20)
    private String city;
    @Column(length = 60)
    private String fax;
  
    @Column(name = "MSTR_ID", length = 6)
    private String mstrId;
    @Column(name = "NAME_1", length = 45)
    private String name1;
    @Column(length = 60)
    private String phone;
    @Column(name = "SMG_ALTERNATE_PHONE", length = 60)
    private String smgAlternatePhone;
    @Column(name = "SMG_CAREGIVER_SITE_USE_ID", nullable = false)
    private Long smgCaregiverSiteUseId;
    @Column(name = "SMG_DNC", length = 1)
    private String smgDnc;
    @Column(name = "SMG_DNF", length = 1)
    private String smgDnf;
    @Column(name = "SMG_KCI_CERTIFIED_FLAG")
    private String smgKciCertifiedFlag;
    @Column(name = "SMG_KCIDOCS_WEB_ACTIVE", length = 1)
    private String smgKcidocsWebActive;
    @Column(length = 2)
    private String state;
    @Column(name = "ZIP_CODE", length = 5)
    private String zipCode;

    public XxkciPegaCaregiverIdsV() {
    }

    public XxkciPegaCaregiverIdsV(String add1, String add2, String addDesc, String addType, String busType, String city,
                                  String fax, String mstrId, String name1, String phone, String smgAlternatePhone,
                                  Long smgCaregiverSiteUseId, String smgDnc, String smgDnf, String smgKciCertifiedFlag,
                                  String smgKcidocsWebActive, String state, String zipCode) {
        this.add1 = add1;
        this.add2 = add2;
        this.addDesc = addDesc;
        this.addType = addType;
        this.busType = busType;
        this.city = city;
        this.fax = fax;
        this.mstrId = mstrId;
        this.name1 = name1;
        this.phone = phone;
        this.smgAlternatePhone = smgAlternatePhone;
        this.smgCaregiverSiteUseId = smgCaregiverSiteUseId;
        this.smgDnc = smgDnc;
        this.smgDnf = smgDnf;
        this.smgKciCertifiedFlag = smgKciCertifiedFlag;
        this.smgKcidocsWebActive = smgKcidocsWebActive;
        this.state = state;
        this.zipCode = zipCode;
    }

    public String getAdd1() {
        return add1;
    }

    public void setAdd1(String add1) {
        this.add1 = add1;
    }

    public String getAdd2() {
        return add2;
    }

    public void setAdd2(String add2) {
        this.add2 = add2;
    }

    public String getAddDesc() {
        return addDesc;
    }

    public void setAddDesc(String addDesc) {
        this.addDesc = addDesc;
    }

    public String getAddType() {
        return addType;
    }

    public void setAddType(String addType) {
        this.addType = addType;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getId() {
        return ID;
    }

    public void setId(String id) {
        this.ID = id;
    }

    public String getMstrId() {
        return mstrId;
    }

    public void setMstrId(String mstrId) {
        this.mstrId = mstrId;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSmgAlternatePhone() {
        return smgAlternatePhone;
    }

    public void setSmgAlternatePhone(String smgAlternatePhone) {
        this.smgAlternatePhone = smgAlternatePhone;
    }

    public Long getSmgCaregiverSiteUseId() {
        return smgCaregiverSiteUseId;
    }

    public void setSmgCaregiverSiteUseId(Long smgCaregiverSiteUseId) {
        this.smgCaregiverSiteUseId = smgCaregiverSiteUseId;
    }

    public String getSmgDnc() {
        return smgDnc;
    }

    public void setSmgDnc(String smgDnc) {
        this.smgDnc = smgDnc;
    }

    public String getSmgDnf() {
        return smgDnf;
    }

    public void setSmgDnf(String smgDnf) {
        this.smgDnf = smgDnf;
    }

    public String getSmgKciCertifiedFlag() {
        return smgKciCertifiedFlag;
    }

    public void setSmgKciCertifiedFlag(String smgKciCertifiedFlag) {
        this.smgKciCertifiedFlag = smgKciCertifiedFlag;
    }

    public String getSmgKcidocsWebActive() {
        return smgKcidocsWebActive;
    }

    public void setSmgKcidocsWebActive(String smgKcidocsWebActive) {
        this.smgKcidocsWebActive = smgKcidocsWebActive;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
